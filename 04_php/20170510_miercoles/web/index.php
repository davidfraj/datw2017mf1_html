<?php  
session_start();
//CONFIGURACION
$rutaBase='ficheros/'; //Configuro la ruta Base de mis ficheros
$tamaRutaBase=strlen($rutaBase); //Cuant letras tiene la $rutaBase

//Recojo la pagina central que quiero mostrar
if(isset($_GET['p'])){
  $p=$_GET['p'];
}else{
  $p='inicio.php';
}
//Fin de recoger la pagina central que mostrare

//Recojo la RUTA del directorio, donde hare las operaciones de listas, subir, crear otros ...
if(isset($_GET['ruta'])){
  $ruta=$_GET['ruta'];
}else{
  $ruta=$rutaBase;
}
//Fin de recoger la ruta

//Vamos a comprobar que NUNCA nos salimos del directorio 
//por defecto
//substr("hola que tal", 0, 6); //hola q
if(substr($ruta, 0, $tamaRutaBase)==$rutaBase){
  $rutaCorrecta=true;
}else{
  $rutaCorrecta=false;
}
//si encontramos algun ".." en la ruta, NO DEJAMOS ACCEDER
if(strpos($ruta, '..')){
  $rutaCorrecta=false;
}

//Si la ruta es correcta.....
if($rutaCorrecta){
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Proyecto BootStrap 01</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Theme -->
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
    
    <!-- Cargo LIGHTBOX2 -->
    <link href="css/lightbox.min.css" rel="stylesheet">

  </head>
  <body>
    <section class="container">
      <header class="row">
        <div class="col-md-8">
          <?php require('includes/encabezado.php'); ?>
        </div>
         <div class="col-md-4">
          <?php require('includes/login.php'); ?>
        </div>
      </header>
      <nav>
        <?php require('includes/menu.php'); ?>
      </nav>
      <section>
        <?php require('paginas/'.$p); ?>
      </section>
      <footer>
        <?php require('includes/pie.php'); ?>
      </footer>
    </section>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below) -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Cargo El Javascript de LightBox -->
    <script src="js/lightbox.min.js"></script>
  </body>
</html>
<?php  
}else{
  echo 'Has intentado poner una ruta a mano..... espabilaoooo';
}
?>