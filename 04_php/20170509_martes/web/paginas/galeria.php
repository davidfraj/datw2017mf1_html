<br>
<h3>
	Galeria de imagenes
	<small>Listado de imagenes del directorio</small>
</h3>

<?php require('includes/migasdepan.php'); ?>
<br>

<form action="index.php?p=galeria.php&ruta=<?php echo $ruta;?>" method="post">
	<div class="form-group">
		<label for="directorio">Crear un nuevo directorio:</label>
		<input type="text" name="directorio" id="directorio">
		<input type="submit" value="Crear directorio" name="enviar" class="btn btn-default">
	</div>
</form>

<hr>
<?php  
//Voy a evaluar, si el usuario ha pulsado el enlace de borrar
if(isset($_GET['borrar'])){
	if(is_file($ruta.$_GET['borrar'])){
		unlink($ruta.$_GET['borrar']);
	}
}

//Voy a evaluar, si el usuario QUIERE crear un directorio
if(isset($_POST['enviar'])){
	$directorio=$_POST['directorio'];
	if(!is_dir($ruta.$directorio)){
		mkdir($ruta.$directorio);
	}
}

//Voy a evaluar, si el usuario QUIERE borrar un directorio
if(isset($_GET['borrardirectorio'])){
	if(is_dir($ruta)){
		rmdir($ruta);
		$partes=explode("/", $ruta);
		array_pop($partes);
		array_pop($partes);
		$ruta=implode("/",$partes).'/';
		header('Location:index.php?p=galeria.php&ruta='.$ruta);
	}
}

//Abro un recurso que sera un directorio
$dir=opendir($ruta);
//Me creo un contador de elementos
$numElementos=0;
//Recorro este recurso, para listar su contenido
while($elemento=readdir($dir)){
	if(is_file($ruta.$elemento)){
		$numElementos++;
		?>
		<figure style="display: inline-block; vertical-align: top;" class="text-center">
			
			<a href="<?php echo $ruta.$elemento;?>" data-lightbox="galeria">
			<img class="img img-rounded" src="<?php echo $ruta.$elemento;?>" width="150">
			</a>

			<figcaption class="text-center">
				<?php echo $elemento;?>
				 - 
				 <a href="index.php?p=galeria.php&borrar=<?php echo $elemento;?>&ruta=<?php echo $ruta;?>" onClick="if(!confirm('Estas seguro?')){return false;};">
				 	<span class="glyphicon glyphicon-remove"></span>
				 </a>
			</figcaption>

		</figure>
		<?php
	}else{
		//Si estamos aqui.... es que hemos encontrado un directorio
		if(($elemento!=".")AND($elemento!="..")){
		$numElementos++;	
		?>
		<a href="index.php?p=galeria.php&ruta=<?php echo $ruta.$elemento.'/';?>">
		<figure style="display: inline-block; vertical-align: top;" class="text-center">
			<img class="img img-rounded" src="imagenes/directorio.jpg" width="150">
			<figcaption class="text-center">
				<?php echo $elemento;?>
			</figcaption>
		</figure>
		</a>
		<?php
		}
	}
}
//Cierro el recurso del directorio
closedir($dir);

//Si el $numElementos es 0, muestro un mensaje como que NO hay elementos
if($numElementos==0){
	?>
	<p>
		No se han encontrado elementos
		 - 
		<a href="index.php?p=galeria.php&borrardirectorio=si&ruta=<?php echo $ruta;?>" onClick="if(!confirm('Estas seguro?')){return false;};">
			¿Borrar directorio?
		</a>
	</p>
	<?php
}


?>