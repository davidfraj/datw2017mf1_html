<?php
//Si fuera necesario.... declaro la variable de session
if(!isset($_SESSION['conectado'])){
  $_SESSION['conectado']=null;
}

//Posibilidad de que el usuario se conecte
if(isset($_POST['entrar'])){
  $email=$_POST['email'];
  $password=$_POST['password'];
  if(($email=='davidfraj@gmail.com')AND($password=='1234')){
    $_SESSION['conectado']=true;
  }
}

//Posibilidad de que el usuario se desconecte
if(isset($_GET['desconectar'])){
  $_SESSION['conectado']=null;
}

//Posibilidad de comprobar si estamos o no conectados
if($_SESSION['conectado']==true){
  ?>
  Bienvenido administrador
  <br>
  <a href="index.php?desconectar=si">Desconectar</a>
  <?php
}else{
?>

<br>
<form class="form" role="form" action="index.php" method="post">
  <div class="form-group">
    <label class="sr-only" for="email">Email</label>
    <input type="email" class="form-control" id="email"
           placeholder="Introduce tu email" name="email">
  </div>
  <div class="form-group">
    <label class="sr-only" for="password">Contraseña</label>
    <input type="password" class="form-control" id="password" 
           placeholder="Contraseña" name="password">
  </div>
  <div class="checkbox">
    <label>
      <input type="checkbox" name="mantener"> No cerrar sesión
    </label>
    <button type="submit" class="btn btn-default" name="entrar">Entrar</button>
  </div>
</form>

<?php
}
?>