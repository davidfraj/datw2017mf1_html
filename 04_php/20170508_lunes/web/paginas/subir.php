<br>
<h3>
	Subir imagenes
	<small>Upload de imagen al servidor</small>
</h3>
<br>

<?php  
//Queremos recoger los DATOS que vienen del formulario
if(isset($_POST['enviar'])){
	//$_FILES['archivo']['name'];//Nombre ORIGINAL del archivo
	//$_FILES['archivo']['tmp_name'];//Ubicacion temporal del archivo
	//$_FILES['archivo']['size'];//Tamaño en bytes
	//$_FILES['archivo']['type'];//Tipo de archivo 'EJ: image/jpeg'
	//$_FILES['archivo']['error'];//Posible error
	//Como sabemos recoger el fichero

	$nombre=time().'_'.$_FILES['archivo']['name'];
	$tipo=$_FILES['archivo']['type'];

	if(($tipo=='image/jpeg')OR($tipo=='image/png')){

		if(move_uploaded_file($_FILES['archivo']['tmp_name'], 'ficheros/'.$nombre)){
			?>
			<div class="alert alert-success alert-dismissable">
			  <button type="button" class="close" data-dismiss="alert">&times;</button>
			  <strong>OK!</strong> Imagen subida con exito.
			</div>
			<?php
			
			//Cuando llega a este punto.... REDIRECCIONA A la galeria
			header('location:index.php?p=galeria.php');

		}else{
			?>
			<div class="alert alert-danger alert-dismissable">
			  <button type="button" class="close" data-dismiss="alert">&times;</button>
			  <strong>ERROR!</strong> Error al subir la imagen.
			</div>
			<?php
		}

	}else{

		?>
			<div class="alert alert-warning alert-dismissable">
			  <button type="button" class="close" data-dismiss="alert">&times;</button>
			  <strong>OJO!</strong> el archivo no es del tipo correcto.
			</div>
		<?php

	}


}
?>

<form action="index.php?p=subir.php" method="post" enctype="multipart/form-data">
	
	<div class="form-group">
		<label for="archivo">Archivo de imagen a subir:</label>
		<input type="file" name="archivo" id="archivo" class="btn btn-default">

		<br><br>

		<input type="submit" value="Subir archivo" name="enviar" class="btn btn-default">
	</div>

</form>
