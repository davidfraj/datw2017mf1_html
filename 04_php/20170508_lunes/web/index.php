<?php  
//Recojo la pagina central que quiero mostrar
if(isset($_GET['p'])){
  $p=$_GET['p'];
}else{
  $p='inicio.php';
}
//Fin de recoger la pagina central que mostrare
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Proyecto BootStrap 01</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Theme -->
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">

  </head>
  <body>
    <section class="container">
      <header>
        <?php require('includes/encabezado.php'); ?>
      </header>
      <nav>
        <?php require('includes/menu.php'); ?>
      </nav>
      <section>
        <?php require('paginas/'.$p); ?>
      </section>
      <footer>
        <?php require('includes/pie.php'); ?>
      </footer>
    </section>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>