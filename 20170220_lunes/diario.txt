Lunes 20 de Febrero de 2017
----------------------------

Programación y desarrollo de paginas web.

	Programación de cara al cliente (Navegador)
		Chrome, Firefox, safari, Internet explorer, opera, ....
		
		
	Programación de cara al servidor (Servidores web)
		Apache, LighHttp, IIS, Tomcat....
		
	Lenguaje de programación es INTERPRETADO por alquien
	
		
	MAQUINA ---BINARIO------INTERPRETE------C++, PHP, HTML---> PERSONA
		
								NAVEGADOR			HTML
								NAVEGADOR			CSS
								NAVEGADOR			JAVASCRIPT
									SERVIDOR			PHP
								NAVEGADOR			JQUERY
									SERVIDOR			SQL
	
	------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------
	Programación de cara al cliente (Navegador)
		Chrome, Firefox, safari, Internet explorer, opera, ....
		
		Que necesitamos para programar
			
			Bloc de notas (Windows)
			Notepad++
			Brackets
			Sublime text 3 (Plugins)
				https://www.sublimetext.com/
					NOTA: Al instalaralo, ACTIVAR la casilla que pone ADD TO CONTEXT MENU

			A partir de ahora, usaremos siempre este programa.
				
			TODAS LAS PAGINAS WEB, se CREAN CON LENGUAJE DE ETIQUETAS HTML

				HTML -> Hypertext markup language	
					<p>
						<i>Hola que tal</i>, hoy el <b>lunes</b>	
					</p>

				XML, RSS, XHTML, WAP...
				Lenguaje HTML5 

					Estructurar los contenidos de la web
						Tabla, Formulario, imagen, enlaces, videos, Mapa...

				CSS -> Cascading style sheet (Hoja de estilos en cascada)

				---> Programadores de FRONTEND

				Ni HTML, ni CSS, son lenguajes de programación. Html es de marcas, y css de estilos


				JAVASCRIPT (Inventado por el navegador NETSCAPE)
		
					Es un lenguaje de programación de verdad..... interpretado por el cliente, que ofrece INTERACTIVIDAD con el usuario
		
				JQuery -> Es un Framework de JAVASCRIPT, para facilitarnos la vida
		
			Si quiero programar de cara a paginas Responsivas (Paginas web adepatadas a movil, tablet y ordenador)

				BootStrap -> Es un Framework de trabajo para HTML, CSS y JS
		
			NOTA: Hay que tener en cuenta, que CADA navegador, interpreta el codigo de cliente como quiere.....
													
		

		------------------------------------------------------------------------------------
		------------------------------------------------------------------------------------
		Programación de cara al servidor (Servidores web)
		Apache, LighHttp, IIS, Tomcat....
		
			PHP ->  Hypertext Preprocessor (Codigo abierto..) -> Que más se emplea en el mundo.
				Servidor Apache
				Base de datos Mysql (MariaDB)

			ASP -> Active Server Pages (Microsoft) .NET
				Servidor Microsoft IIS
				Base de datos Sql Server

			JSP -> Java Server Pages
				Servidor Tomcat
				Base de datos Oracle

			CMS (Content management system) en el mercado : Wordpress, joomla, Prestashop, Moodle, vbBulletin, OwnCloud, Mambo y Drupal

			---------------

			Para qué sirve un lenguaje de servidor:

				No sirve para hacer imagenes

				No sirve para texto

				No sirve, para enlaces...

				No sirve para hacer páginas web...

			Sirve para generar CODIGO de otros lenguajes

			Acceder a una base de datos

			Mandar correo electronico

			Autenticarse, con usuario y clave...

			......	


		--
			Sistemas de base de datos
			UML, MER, ....

		--
			Configuración de servidores web

		-- 
			HTTP y HTTPS (certificados)

		--
			Modelo vista controlador
			symfony
			doctrine
			composer y github



